#!/usr/bin/python3
from keepassexcnage.app import app as application

if __name__ == "__main__":
    application.run(debug=True)
