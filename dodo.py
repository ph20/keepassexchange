#!/usr/bin/env python3

import os
import shutil
import urllib.request
import sys

print('Python %s on %s' % (sys.version, sys.platform))

ROOT_DIR = os.path.dirname(__file__)
BOWER_COMP = os.path.join(ROOT_DIR, 'bower_components')
STATIC_LIB = os.path.join(ROOT_DIR, 'static', 'lib')
DST_MAP = {
    '.css': 'css',
    '.js':  'js',
    '.gif': 'img',
    '.svg': 'img'
}


def task_npm_run_build():
    """npm run build"""

    return {
        'actions': ['npm install', 'npm run build'],
    }


def get_url(src, dst):
    req = urllib.request.Request(
        src,
        data=None,
        headers={
            'User-Agent':
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) '
                'Chrome/35.0.1916.47 Safari/537.36'
        }
    )

    fsrc = urllib.request.urlopen(req)
    with open(dst, 'wb') as fdst:
        shutil.copyfileobj(fsrc, fdst)


def task_deploy_static():
    """copy needed js css and img files to static directory"""
    def setup_web_component(targets):
        for src in targets:
            dst_dir = os.path.join(STATIC_LIB, DST_MAP[os.path.splitext(src)[-1]])
            if not os.path.exists(dst_dir):
                os.makedirs(dst_dir)
            dst = os.path.join(dst_dir, os.path.basename(src))
            if src.startswith('https://') or src.startswith('http://'):
                get_url(src, dst)
            else:
                shutil.copy(os.path.join(BOWER_COMP, src), dst)
            print('{} => {}'.format(src, dst))

    return {
        'actions': [setup_web_component],
        'targets': ['bootstrap/dist/css/bootstrap.css',
                    'bootstrap/dist/js/bootstrap.js',
                    'jquery/dist/jquery.js',
                    'clipboard/dist/clipboard.js',
                    'https://clipboardjs.com/assets/images/clippy.svg']
    }


if __name__ == '__main__':
    import doit
    doit.run(globals())
