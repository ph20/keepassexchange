import os


class BaseConfig(object):
    STORAGE_ID_LEN = 12
    STORAGE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'storage'))
    CUSTOM_CONFIG = ''
    AWS_ACCESS_KEY_ID = ''
    AWS_SECRET_ACCESS_KEY = ''
