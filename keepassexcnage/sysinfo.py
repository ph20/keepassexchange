import os
import sys
import socket
import platform
import pwd
import re


def cpu_info():
    info = 'Cpu info fro /proc/cpuinfo\n'
    with open('/proc/cpuinfo') as f:
        for line in f:
            info += line.rstrip('\n') + '\n'
    return info


def getusers():
    info = '# Get the users from /etc/passwd\n'
    users = pwd.getpwall()
    for user in users:
        info += '{0}:{1}\n'.format(user.pw_name, user.pw_shell)
    return info


def proc_list():
    cmdlines = list()
    for pid in (_ for _ in os.listdir('/proc') if _.isdigit()):
        try:
            with open(os.path.join('/proc', pid, 'cmdline')) as cmdline_:
                cmdline_ = cmdline_.read().strip()
                if cmdline_:
                    cmdlines.append(cmdline_)
        except IOError:  # proc has already terminated
            continue
    return cmdlines


def get_cmd(pid_):
    with open(os.path.join('/proc/{}/cmdline'.format(pid_))) as cmdline_:
        cmdline_ = cmdline_.read().strip()
    return cmdline_


def self_pid_info():
    pid_ = os.getpid()
    return pid_, get_cmd(pid_)


def parend_pid(pid_):
    stat_content = ''
    try:
        stat_path = '/proc/{}/stat'.format(pid_)
        with open(stat_path) as cmdline_:
            stat_content = cmdline_.read()
            parent_pid_match = re.search('\d+\s+\(.+\)\s+\S+\s+(\d+)', stat_content)
            if parent_pid_match:
                parent_pid = int(parent_pid_match.group(1))
            else:
                parent_pid = 0
        return parent_pid
    except Exception as e:
        print("Can't obtain parent pid from file {}. {}.\n{}".format(stat_path, e, stat_content))
        return 0


def proc_stack():
    pid_ = os.getpid()
    info = []
    while True:
        cmd = get_cmd(pid_)
        info.append((pid_, cmd))
        pid_ = parend_pid(pid_)
        if not pid_:
            return info


def gather_environment_information(flask_request, flask_app):
    env_string = '# python_version: {}\n'.format(platform.python_version())
    env_string += '# python_executable: {}\n'.format(sys.executable)
    env_string += '# hostname: {}\n'.format(socket.gethostname())
    env_string += '# fqdn: {}\n'.format(socket.getfqdn())
    env_string += '# platform name: {}\n'.format(platform.system().lower())
    env_string += '# platform release: {}\n'.format(platform.release().lower())
    env_string += '# platform version: {}\n'.format(platform.version().lower())
    env_string += '# platform fullname: {}\n'.format(platform.platform())
    env_string += '# platform endian: {}\n'.format(sys.byteorder)

    env_string += '>>> os.getcwd()\n{}\n\n'.format(os.getcwd())
    env_string += '>>> os.environ\n{}\n\n'.format('\n'.join(["    {}: '{}'".format(*_) for _ in os.environ.items()]))
    env_string += '>>> sys.path\n{}\n\n'.format('\n'.join(['    ' + _ for _ in sys.path]))
    env_string += '>>> app.config\n{}\n\n'.format('\n'.join(["    {}: '{}'".format(*_) for _ in flask_app.config.items()]))
    env_string += '>>> request.headers\n{}\n\n'.format('\n'.join(["    {}: '{}'".format(*_) for _ in flask_request.headers.items()]))
    env_string += '{}\n\n'.format(cpu_info())
    env_string += '{}\n\n'.format(getusers())
    env_string += 'Processes stack:\n{}\n\n'.format(''.join(['{} : {}\n'.format(*_) for _ in proc_stack()]))
    return env_string