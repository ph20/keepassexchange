import os
from flask import send_file
import shutil
import random
import string
import logging
import boto3
from botocore.exceptions import ClientError

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('s3transfer').setLevel(logging.CRITICAL)

STORAGE_ID_LEN = 12
DELETE_CCODE_FILE = '.delpasswd'
DELETE_CODE_LEN = 8


class StorageError(Exception):
    pass


class StoragePoolAbsentError(StorageError):
    pass


class StorageExistError(StorageError):
    pass


class NoSuchStorage(StorageError):
    pass


def random_storage_id():
    return ''.join(random.choice(string.digits) for _ in range(STORAGE_ID_LEN))


def random_del_passwd():
    return ''.join(random.choice(string.digits + string.ascii_lowercase) for _ in range(DELETE_CODE_LEN))


class BaseStorage(object):
    @classmethod
    def set_pool(cls, pool):
        cls._pool = pool

    def __init__(self, id_, create=False):
        self.id_ = id_
        self.file_name = None
        self.file_name_tmp = None
        self.log = logger
        self._del_code_cache = None

        if create:
            self._create()
        else:
            if not self.exist():
                raise NoSuchStorage()

    def _storage_id_to_path(self):
        return os.path.join(self.id_[0], self.id_[1], self.id_[2:])

    def _get_file_path(self, filename):
        return os.path.join(self._storage_id_to_path(), filename)

    def _create(self):
        raise NotImplementedError()

    def get_del_code(self):
        raise NotImplementedError()

    def exist(self):
        raise NotImplementedError()

    def del_storage(self, del_passwd):
        raise NotImplementedError()

    def put(self, stream, filename):
        raise NotImplementedError()

    def get(self, filename):
        raise NotImplementedError()

    def delete(self, filename):
        raise NotImplementedError()

    def move(self, src, dst):
        raise NotImplementedError()


class Storage(BaseStorage):

    def _get_full_path(self, filename=''):
        return os.path.join(self._pool.path, self._get_file_path(filename))

    def _del_code_path(self):
        return os.path.join(self._get_full_path(), DELETE_CCODE_FILE)

    def _create(self):
        if not os.path.exists(self._pool.path):
            raise StoragePoolAbsentError()
        storage_path = self._get_full_path()
        if os.path.exists(storage_path):
            raise StorageExistError()
        else:
            os.makedirs(storage_path)
            with open(self._del_code_path(), 'w') as del_passwd_file:
                del_code_string = random_del_passwd()
                del_passwd_file.write(del_code_string)
                self._del_code_cache = del_code_string

    def get_del_code(self):
        if self._del_code_cache:
            return self._del_code_cache
        else:
            with open(self._del_code_path()) as _:
                self._del_code_cache = _.read()
                return self._del_code_cache

    def put(self, stream, filename, mimetype='application/octet-stream'):
        buf_len = 16 * 1024
        with open(self._get_full_path(filename), 'wb') as fdst:
            while 1:
                buf = stream.read(buf_len)
                if not buf:
                    break
                fdst.write(buf)

    def get(self, filename):
        file_path = self._get_full_path(filename)
        self.log.info('open and return file {}'.format(file_path))
        return send_file(filename_or_fp=file_path)

    def delete(self, filename):
        file_path = self._get_full_path(filename)
        os.unlink(file_path)
        self.log.info('file {} deleted'.format(filename))

    def move(self, src, dst):
        shutil.move(self._get_full_path(src), self._get_full_path(dst))
        self.log.info('file {} moved to {}'.format(src, dst))

    def exist(self, filename=''):
        return os.path.exists(self._get_full_path(filename))

    def del_storage(self, del_passwd):
        if del_passwd == self.get_del_code():
            shutil.rmtree(self._get_full_path())
            self.log.info('Storage "{}" was deleted'.format(self.id_))


class StorageS3(BaseStorage):

    def _obj(self, filename):
        file_path = self._get_file_path(filename)
        return self._pool.bucket.Object(self._get_file_path(filename)), self._pool.bucket_url + file_path

    def _check_exist(self, filename):
        try:
            obj, url = self._obj(filename)
            self.log.debug('Check present {}'.format(url))
            obj.load()
            return True
        except ClientError as e:
            if e.response['Error']['Code'] == "404":
                return False
            else:
                raise

    def exist(self):
        return self._check_exist(filename=DELETE_CCODE_FILE)

    def _save_string(self, filename, string):
        obj, url = self._obj(filename)
        self.log.debug('Save string {} to {}'.format('*'*min([DELETE_CODE_LEN, len(string)]), url))
        obj.put(Body=string.encode(), ContentType='text/plain')

    def _create(self):
        if self.exist():
            raise StorageExistError()
        else:
            del_code_string = random_del_passwd()
            self._save_string(filename=DELETE_CCODE_FILE, string=del_code_string)
            self._del_code_cache = del_code_string

    def get_del_code(self):
        obj, url = self._obj(DELETE_CCODE_FILE)
        self.log.debug('Get deleting code from {}'.format(url))
        return obj.get()['Body'].read(DELETE_CODE_LEN).decode()

    def put(self, stream, filename, mimetype='application/octet-stream'):
        obj, url = self._obj(filename)
        self.log.debug('Save file to {}'.format(url))
        obj.put(Body=stream, ContentType=mimetype)

    def get(self, filename):
        obj, url = self._obj(filename)
        obj_resp = obj.get()
        self.log.debug('Obtaininng file from {}'.format(url))
        return send_file(filename_or_fp=obj_resp['Body'], mimetype=obj_resp['ContentType'])

    def delete(self, filename):
        obj, url = self._obj(filename)
        self.log.debug('Deleting file {}'.format(url))
        obj.delete()

    def move(self, src, dst):
        src_obj, url_src = self._obj(src)
        dst_obj, url_dst = self._obj(dst)
        self.log.info('file {} moved to {}'.format(url_src, url_dst))
        dst_obj.copy({'Bucket': src_obj.bucket_name, 'Key': src_obj.key})
        src_obj.delete()

    def del_storage(self, del_passwd):
        if del_passwd == self.get_del_code():
            for obj in self._pool.bucket.objects.filter(Prefix=self._get_file_path(filename='')):
                obj.delete()
            self.log.info('Storage "{}" was deleted'.format(self.id_))


class StoragePool(object):

    def __init__(self, path):
        if path.startswith('s3://'):
            self.storage_cls = StorageS3
            self.bucket, self.bucket_url = self._get_bucket(bucket_name=path[len('s3://'):])

        else:
            self.storage_cls = Storage
            self.path = path
            if not os.path.exists(self.path):
                raise StoragePoolAbsentError()

        self.storage_cls.set_pool(self)

    def _get_bucket(self, bucket_name):
        aws_access_key_id = os.environ.get('AWS_ACCESS_KEY_ID')
        if aws_access_key_id is None:
            raise RuntimeError('environment variable AWS_ACCESS_KEY_ID absent')
        aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
        if aws_secret_access_key is None:
            raise RuntimeError('environment AWS_SECRET_ACCESS_KEY  absent')

        s3resource = boto3.resource('s3',
                                    aws_access_key_id=aws_access_key_id,
                                    aws_secret_access_key=aws_secret_access_key)
        available_buckets = []
        for bucket in s3resource.buckets.all():
            available_buckets.append(bucket.name)
            if bucket.name == bucket_name:
                client = boto3.client('s3', aws_access_key_id=aws_access_key_id,
                                      aws_secret_access_key=aws_secret_access_key)
                location = client.get_bucket_location(Bucket=bucket_name)['LocationConstraint']
                location_url = "https://s3-{0}.amazonaws.com/{1}/".format(location, bucket_name)
                return bucket, location_url
        raise StoragePoolAbsentError('Can\'t find bucked with name "{}". Available bucket(s) are: {}'.format(
            bucket_name, available_buckets))

    def get(self, id_):
        return self.storage_cls(id_=id_)

    def create(self):
        for _ in range(10):
            storage_id = random_storage_id()
            try:
                return self.storage_cls(id_=storage_id, create=True)
            except StorageExistError:
                continue
        raise RuntimeError("Can't create storage")
