"""
Collection funtion for interrapt eith Run Time enfironment
"""
import os

RUN_ENV_OPENSHIFT = 'openshift'
RUN_ENV_PYCHARM = 'pycharm'

RUN_ENVIRONMENT_ENV = 'RUN_ENVIRONMENT'


def inside_openshift():
    return 'KUBERNETES_PORT' in os.environ and 'OPENSHIFT_BUILD_NAME' in os.environ


RUN_ENVIRONMENTS = {
    RUN_ENV_OPENSHIFT: inside_openshift,


}


def run_environment():
    if RUN_ENVIRONMENT_ENV in os.environ:
        env_name = os.environ[RUN_ENVIRONMENT_ENV]
        if env_name not in RUN_ENVIRONMENTS:
            raise RuntimeError('Not supported runtime environment "{}"'.format(env_name))
        return env_name

