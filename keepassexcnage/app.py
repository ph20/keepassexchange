import os
import sys
from flask import Flask
from flask import request
from flask import send_file, abort
from flask import render_template
from flask import jsonify
from flask import Response
from werkzeug.utils import secure_filename

from urllib import parse as urlparse
import shutil
import logging


from keepassexcnage.conf import BaseConfig
from keepassexcnage.storage import StoragePool, STORAGE_ID_LEN
from keepassexcnage.sysinfo import gather_environment_information

DEFAULT_DIR = os.path.dirname(__file__)
TEMPLATE_FOLDER = os.path.join(DEFAULT_DIR, '..', 'templates')
STATIC_FOLDER = os.path.join(DEFAULT_DIR, '..', 'static')

# init all needed classes
app = Flask('keepassexchange', template_folder=TEMPLATE_FOLDER, static_folder=STATIC_FOLDER)

# loading base config
app.config.from_object(BaseConfig)
# load custom config
custom_conf_env = os.environ.get('CUSTOM_CONFIG')
if custom_conf_env and os.path.exists(custom_conf_env):
    app.config.from_json(custom_conf_env)
    app.config['CUSTOM_CONFIG'] = custom_conf_env

storage_pool = StoragePool(os.environ.get('STORAGE_PATH', app.config['STORAGE_PATH']))

logger = logging.getLogger('keepassexchange')
logger.setLevel(logging.DEBUG)
logging.basicConfig(level=logging.DEBUG)


@app.route('/<int:storage_id>/<filename>', methods=['GET', 'PUT', 'DELETE', 'MOVE'])
def exchange(storage_id, filename):
    storage_id_ = str(storage_id)
    if len(storage_id_) != STORAGE_ID_LEN:
        abort(404)
    storage_obj = storage_pool.get(id_=storage_id_)
    if filename != secure_filename(filename):
        app.logger.warning('Detected unsecure file name {}'.format(repr(filename)))
        abort(404)

    if request.method == 'GET':
        file_obj = storage_obj.get(filename)
        return file_obj

    elif request.method == 'PUT':
        storage_obj.put(stream=request.data, filename=filename)
        return jsonify({'result': 'ok'})

    elif request.method == 'DELETE':
        storage_obj.delete(filename=filename)
        return jsonify({'result': 'ok'})

    elif request.method == 'MOVE':
        # extract and validate destination path
        full_url_path = urlparse.urlparse(request.headers['DESTINATION']).path
        filename_dst = os.path.basename(full_url_path)
        storage_obj.move(src=filename, dst=filename_dst)
        return jsonify({'result': 'ok'})


@app.route('/', methods=['GET'])
@app.route('/home', methods=['GET'])
def home():
    return render_template('index.html')


@app.route('/create', methods=['GET'])
def create_iframe():
    return render_template('uploadiframe.html')


@app.route('/create', methods=['POST'])
def create_action():
    # check if the post request has the file part
    files_ = list(request.files.values())
    if len(files_) != 1:
        return jsonify({'msg': 'No file', 'result': 'error'})
    file = files_[0]
    if file.filename == '':
        return jsonify({'msg': 'No selected file', 'result': 'error'})
    if not file:
        return jsonify({'msg': 'No file', 'result': 'error'})
    file_name = secure_filename(file.filename)
    storage = storage_pool.create()
    storage.put(stream=file.stream, filename=file_name, mimetype=file.content_type)
    sync_url = request.host_url.rstrip('/') + '/' + storage.id_ + '/' + file_name
    del_url = request.host_url.rstrip('/') + '/delete/' + storage.id_
    return jsonify({'url': sync_url, 'delpasswd': storage.get_del_code(), 'delurl': del_url, 'result': 'ok'})


@app.route('/delete/<int:storage_id>', methods=['POST'])
def delete_action(storage_id):
    storage_id_ = str(storage_id)
    if len(storage_id_) != STORAGE_ID_LEN:
        abort(404)
    storage_obj = storage_pool.get(id_=storage_id_)
    del_passwd = request.form.get('delpasswd')
    if del_passwd is None:
        abort(404)
    else:
        storage_obj.del_storage(del_passwd=del_passwd)
        return jsonify({'result': 'ok'})


@app.route('/env', methods=['GET'])
def show_env():
    env_string = gather_environment_information(request, app)
    return Response(env_string, mimetype='text/plain')


if __name__ == '__main__':
    app.run()
